/*
 * File: BookReader.java
 * Author: Ruvim Kondratyev
 * Date: Sat, 17 Jul 2021
 * 
 * Copyright 2021 by Ruvim Kondratyev.
 * 
 * Written on Saturday, July 17, 2021.
 * The point of this was to create a
 * demo that can be used for applying
 * for mobile developer jobs.
 * 
 */
package com.gradgoose.learner;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/*
 * User story:
 * As a student, I need to be able to read a book from the course lesson so that I can learn the content.
 * 
 * This activity implements the user story.
 * 
 */
public class BookReader extends AppCompatActivity
{
	/**
	 * The tag used for logging.
	 */
	static final String TAG = "CourseActivity";

	/**
	 * The queue capacity for the queue in which we track which images we extract from the ZIP file.
	 * 
	 * Normal use case is to load only 1 image at a time, but we allow a couple extra.
	 * 
	 */
	static final int IMAGE_QUEUE_CAPACITY = 4;

	/**
	 * The buffer size to use for copying files when extracting images from the ZIP file.
	 */
	static final int COPY_BUFFER_SIZE = 4096;

	/**
	 * The .course file (a ZIP file with course contents inside it).
	 */
	File courseFile = null;
	/**
	 * The course content loaded into memory.
	 * 
	 * (Just text though; we never load images into memory unless we're actively displaying them.)
	 * 
	 */
	Util.Course course = null;

	/**
	 * The number (or index, or position) of the item that we're reading.
	 */
	int itemPosition = 0;
	/**
	 * The item that we're reading.
	 */
	Object item = null;

	/**
	 * The Book that we're reading (same as item, but cast to a Book if it's a Book).
	 */
	Util.Book book = null;
	/**
	 * Chapter that we're reading.
	 * 
	 * Chapter number is: 0 <= chapter < N. chapters.
	 * 
	 * Chapter 0 is the first chapter.
	 * Chapter N-1 is the last chapter.
	 * 
	 */
	int chapterNumber = 0;
	/**
	 * Page that we're reading.
	 * 
	 * Page number is: 0 <= chapter <= N. pages in chapter.
	 * THIS IS DIFFERENT FROM CHAPTER NUMBER!
	 * 
	 * Page 0 is the title page for the chapter.
	 * Page 1 is the first page of the chapter.
	 * Page N is the last page of the chapter.
	 * 
	 */
	int pageNumber = 0;

	/**
	 * Page text that goes at the top above the image, if there is an image.
	 */
	TextView tvText;
	/**
	 * The image, if there is an image.
	 */
	ImageView ivImage;
	/**
	 * Title page text.
	 * 
	 * For title pages, we use this instead of the other text element.
	 * 
	 */
	TextView tvTitle;
	/**
	 * Element to hold the page number text.
	 * 
	 * This one might be positioned at the bottom of the UI,
	 * though the Java doesn't care where it's positioned,
	 * the XML cares about that instead.
	 * 
	 */
	TextView tvPageNumber;

	/**
	 * Map of image paths within the ZIP file to image paths in our temporary cache dir.
	 * 
	 * We can't easily give paths to ImageView to files inside the ZIP archive,
	 * so we extract them to temporary files in our cache directory, and this
	 * map is what keeps track of which ZIP path goes to which temporary path
	 * for display on the UI.
	 * 
	 */
	final HashMap<String, String> imagePathMap = new HashMap<> ();

	/**
	 * Flag to keep running, so we can set it to 'false' when the activity is done.
	 */
	boolean keepRunning = true;
	/**
	 * Queue for the image-extraction thread of all the image paths to extract.
	 */
	BlockingQueue<String> pathToLoad = new ArrayBlockingQueue<> (IMAGE_QUEUE_CAPACITY);

	/**
	 * Thread to extract images from the ZIP archive into our temporary cache dir.
	 * 
	 * For future work, we might consider checking (after each .take ()) to see
	 * if the file in question has been extracted already or not yet. Very unlikely,
	 * but there is a possibility that the user might somehow tap NEXT and PREV in
	 * a very quick succession such that there might be two of the same source ZIP
	 * path in our queue to extract. This situation will simply extract the same
	 * image twice to two temporary files instead of one (duplicate copy), and it
	 * will not break anything, it will just work just anyway, but it's an idea
	 * of an optimization for a corner case for sometime in the "future" (whatever
	 * the future holds for this particular app).
	 * 
	 */
	Thread imageLoader = new Thread ()
	{
		@Override
		public void run ()
		{
			while (keepRunning)
			{
				try
				{
					String path = pathToLoad.take ();
					int lastDot = path.lastIndexOf ('.');
					String ext = path.substring (lastDot);
					try
					{
						ZipFile zipFile = new ZipFile (courseFile);
						ZipEntry entry = zipFile.getEntry (path);
						if (entry != null)
						{
							InputStream inputStream = zipFile.getInputStream (entry);
							final File tmpFile = File.createTempFile ("img", ext, getCacheDir ());
							FileOutputStream outputStream = new FileOutputStream (tmpFile);
							byte [] buffer = new byte [COPY_BUFFER_SIZE];
							while (inputStream.available () > 0)
							{
								int bRead = inputStream.read (buffer);
								outputStream.write (buffer, 0, bRead);
							}
							outputStream.close ();
							synchronized (imagePathMap)
							{
								imagePathMap.put (path, tmpFile.getAbsolutePath ());
							}
							runOnUiThread (() -> {
								ivImage.setImageURI (Uri.fromFile (tmpFile));
							});
						}
						else
						{
							Log.w (TAG, String.format ("Could not find entry %s in %s", path, courseFile.toString ()));
						}
						zipFile.close ();
					}
					catch (IOException err)
					{
						Log.e (TAG, "Could not read course file for loading image " + path);
					}
				}
				catch (InterruptedException err)
				{
					Thread.currentThread ().interrupt ();
				}
			}
		}
	};

	/**
	 * Called by Android when this activity is being created or is being restarted
	 * due to a screen orientation change or some other cause.
	 * 
	 * @param savedInstanceState The saved instance state to restore after the change.
	 *                           We don't use this for the demo app, but we would use
	 *                           this in a production app.
	 */
	@Override
	protected void onCreate (Bundle savedInstanceState)
	{
		super.onCreate (savedInstanceState);
		setContentView (R.layout.page);

		Uri data = getIntent ().getData ();
		courseFile = new File (data.getPath ());
		if (!courseFile.exists ())
		{
			Toast.makeText (this, "Course file does not exist", Toast.LENGTH_SHORT).show ();
			finish ();
			return;
		}
		
		Bundle extras = getIntent ().getExtras ();
		if (extras != null)
		{
			itemPosition = extras.getInt ("item");
		}
		
		course = Util.loadCourse (courseFile);
		item = course.findItem (itemPosition);
		if (item instanceof Util.Book)
		{
			book = ((Util.Book) item);
			setTitle (book.name);
		}
		else
		{
			book = null;
		}
		
		tvText = findViewById (R.id.tvText);
		ivImage = findViewById (R.id.ivImage);
		tvTitle = findViewById (R.id.tvTitle);
		tvPageNumber = findViewById (R.id.tvPageNumber);
		
		findViewById (R.id.btnPrev).setOnClickListener ((e) -> prevPage ());
		findViewById (R.id.btnNext).setOnClickListener ((e) -> nextPage ());

		imageLoader.start ();
		
		showPage ();
	}

	/**
	 * Called by Android when the activity is being terminated.
	 */
	@Override
	protected void onStop ()
	{
		keepRunning = false;
		super.onStop ();
	}

	/**
	 * Renders the current page onto the UI.
	 */
	void showPage ()
	{
		if (book != null && chapterNumber < book.chapters.size ())
		{
			Util.BookChapter chapter = book.chapters.elementAt (chapterNumber);
			if (pageNumber == 0)
			{
				// Title page.
				tvText.setText ("");
				tvTitle.setText (chapter.name);
				clearImage ();
			}
			else if (pageNumber <= chapter.pages.size ())
			{
				// Normal page.
				Util.BookPage page = chapter.pages.get (pageNumber - 1);
				tvText.setText (page.text);
				tvTitle.setText ("");
				loadImage (page.img);
			}
		}
		tvPageNumber.setText (
				getString (R.string.pg_number)
				.replace ("[CH]", String.valueOf (chapterNumber))
				.replace ("[PG]", String.valueOf (pageNumber))
		);
	}

	/**
	 * Changes the chapter and page numbers as needed to switch to the next page.
	 */
	void nextPage ()
	{
		if (book != null && chapterNumber < book.chapters.size ())
		{
			if (pageNumber + 1 <= book.chapters.get (chapterNumber).pages.size ())
			{
				pageNumber++;
			}
			else if (chapterNumber + 1 < book.chapters.size ())
			{
				chapterNumber++;
				pageNumber = 0;
			}
		}
		showPage ();
	}

	/**
	 * Changes the chapter and page numnbers as needed to switch to the previous page.
	 */
	void prevPage ()
	{
		if (book != null && chapterNumber < book.chapters.size ())
		{
			if (pageNumber > 0)
			{
				pageNumber--;
			}
			else if (chapterNumber > 0)
			{
				chapterNumber--;
				pageNumber = book.chapters.get (chapterNumber).pages.size ();
			}
		}
		showPage ();
	}

	/**
	 * Loads an image into the UI, or queues it for loading if it still needs to be extracted from the ZIP.
	 * 
	 * @param path The path to the image within the ZIP file.
	 */
	void loadImage (String path)
	{
		Uri found = null;
		synchronized (imagePathMap)
		{
			if (imagePathMap.containsKey (path))
			{
				found = Uri.parse (imagePathMap.get (path));
			}
		}
		if (found != null)
		{
			ivImage.setImageURI (found);
		}
		else
		{
			pathToLoad.add (path);
		}
	}

	/**
	 * Clears the image view in the UI to not show any image.
	 */
	void clearImage ()
	{
		ivImage.setImageURI (null);
	}
}

